/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */


import React from 'react';

import AppNavigator from './src/Screen/Navigation/Navigation';
import { Provider } from 'react-redux';
import configureStore from './src/Redux/store';
const { store } = configureStore()
export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    )
  }
};