import { createSwitchNavigator, createAppContainer, withNavigation } from 'react-navigation'
import ScreenA from '../ScreenA'
import ScreenB from '../ScreenB'


const defaultStackNavOptions = {
    headerShown: false
}
const AppNavigator = createSwitchNavigator({
    MainScreen: {
        screen: ScreenA
    },
   SecondScreen:{
       screen:ScreenB
   }
   
},
    {
        initialRouteName: 'MainScreen',
        defaultNavigationOptions: defaultStackNavOptions
    }
)

export default createAppContainer(withNavigation(AppNavigator))