import React, { Component } from 'react'
import { View, Text, Image, TextInput, TouchableOpacity,ScrollView, Keyboard,StyleSheet } from 'react-native'
import { withNavigation } from 'react-navigation'
import constants from './constants'
import { connect } from 'react-redux';
import axios from 'axios'


class Login extends Component {


    state = {
        isKeyboardOpen: false,
        title: '',
        description: '',
        reason: '',
      
    }
   
    
    validate = () => {
     
        if (this.state.title?.trim() == '') {
            alert("please enter Title");
        }else if(this.state.reason?.trim() == ''){
            alert("please enter Description");
        }else{
           
           
            this.placeOrder();
        }
    };

    placeOrder = async () => {
      
        const endPoint = 'http://15.206.122.102:8000/api/data'
        const { title,reason } = this.state
        const headers = {
           'Content-Type': 'application/json',
           
        }
        const body =
        {
           title:title,
           description:reason,
        }
        await axios
            .post(
                endPoint,
                body,
                {
                    headers
                })
            
                .then(async (response) => {
                  if(response.data.message == "Record saved successfully."){
                    alert(response.data.message)
                   
                  }else{
                    alert(response.data.message)
                  }
                  
                   
                })
               
            .catch((error) => {
                console.log(error.response, "order error")
            })
    }
      
    _keyboardDidShow = () => {
        
        this.setState({ isKeyboardOpen: true })
    }

    _keyboardDidHide = () => {
        this.setState({ isKeyboardOpen: false })
    }
 
render() {
        const { title,description } = this.state
        const { navigation } = this.props

        return (
            <ScrollView>
            <View style={styles.mainContainer}>
               
               <View style={{ width: '100%', height: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 37 }}>
                    <Image source={constants.Images.profile} />
                </View>
             
              <View style={styles.inputContainer}>
                <View>
                     <Text style={{fontSize:20,fontStyle:'italic',fontWeight:'700',color:"#e0ffff",elevation: 30}}>Title:</Text>
                </View>
           
                    <View style={styles.textInputContainer}>
                        <TextInput
                            placeholder={"Title"}
                            onChangeText={(text) => this.setState({ title: text })}
                            placeholderTextColor={constants.Colors.TEXT_COLOR}
                            style={styles.textInputStyle}
                            maxLength={20}
                            returnKeyType="next"
                            autoCapitalize='none'
                            selectionColor={constants.Colors.BLACK}
                        />
                    </View>
                    <Text style={{fontSize:20,fontStyle:'italic',fontWeight:'700',color:"#e0ffff",elevation: 30}}>Description:</Text>
                    <View style={styles.textInputContainer}>
                        <TextInput
                            placeholder={"Description"}
                            onChangeText={(text) => this.setState({ reason: text })}
                            placeholderTextColor={constants.Colors.TEXT_COLOR}
                           style={styles.textDesStyle}
                            multiline={true}
                            maxLength={100}
                            numberOfLines={5}
                            selectionColor={constants.Colors.BLACK}
                        />
                        
                    </View>
                    <TouchableOpacity style={styles.buttonView} onPress={() => this.validate()}>
                   
                        <Text style={styles.buttonText}>PROCEED</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.buttonView} onPress={() => this.props.navigation.navigate('SecondScreen')}>
                   
                   <Text style={styles.buttonText}>Show List</Text>
               </TouchableOpacity>
                </View>
            </View>
            </ScrollView>

        )
    }
}

const styles = StyleSheet.create({
    logoContainer: {
        height: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 30,
        borderBottomRightRadius: 26,
        borderBottomLeftRadius: 26,
        justifyContent: 'center',
        alignItems: "center",
        backgroundColor:constants.Colors.BLACK,
        
    },
    inputContainer: {
       marginTop: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 5,
        height: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 58,
     paddingHorizontal: constants.BaseStyle.DEVICE_WIDTH / 10
    },
 
    textLogin: {
        color: constants.Colors.PRIMARY_COLOR,
        ...constants.Fonts.large,
    },
    buttonLogin: {
        borderRadius: 10,
        borderColor: constants.Colors.PRIMARY_COLOR,
        borderWidth: 1,
        paddingVertical: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 0.8,
        marginHorizontal: (constants.BaseStyle.DEVICE_WIDTH / 100) * 8,
        marginTop: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 5,
        alignSelf: "flex-end",
        width: '25%',
        alignItems: "center"

    },
    appContainer: {
       
        flex: 1
    },
    mainContainer: {
        flex: 1,
        backgroundColor: constants.Colors.TEXT_COLOR,
    },
    darkContainer: {
        flex: 1,
        backgroundColor: constants.Colors.CONTAINER_COLOR,
    },
    textInputContainer: {
        backgroundColor: constants.Colors.LIGHT_WHITE,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center',
        //elevation: 20,
       
        borderRadius: 15,
        marginBottom: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 2,
    },
    textInputStyle: {
        color: constants.Colors.GRAY,
    width: '90%',
     paddingLeft:10,
       
        ...constants.Fonts.largeLight,
        ...Platform.select({
            android: { height: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 7.5 },
            ios: { height: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 6.5 },
        }),
    },
    textDesStyle: {
        color: constants.Colors.GRAY,
        width: '90%',
     paddingLeft:10,
     textAlignVertical:'top',
        
        ...constants.Fonts.largeLight,
       
    },
    buttonView: {
        marginTop: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 3,
        backgroundColor: constants.Colors.WHITE,
        paddingVertical: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 2,
        borderRadius: 33,
       
    },
    buttonContainer: {
        height: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 13,
    },
    buttonText: {
        color: constants.Colors.DARK_GRAY,
        ...constants.Fonts.regularBold,
        textAlign: 'center'
    },
    imageView: {
        position: "absolute",
        top: (constants.BaseStyle.DEVICE_HEIGHT / 100) * 9,
        width: "80%",
        marginHorizontal: (constants.BaseStyle.DEVICE_WIDTH / 100) * 10,
    },
})

export default (withNavigation(Login))