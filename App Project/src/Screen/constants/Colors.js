const COLORS = {
    PRIMARY_COLOR: '#FF7129',
    BLACK: '#242424',
    LIGHT_BLACK: '#3A3A3A',
    LIGHT_GRAY: '#999999',
    DARK_BLUE: '#4C95D0',
    DARK_GRAY: '#323232',
    WHITE: '#FFFFFF',
    GRAY: '#F6F6F6',
    TEXT_COLOR: '#5E5E5E',
    BUTTON_LIGHT: 'rgba(94, 94, 94, 0.5)',
    LIGHT_WHITE: 'rgba(255, 255, 255, 0.5)',
    BUTTON_TEXT: '#CCCCCC',
    BORDER_COLOR: "rgba(47, 47, 43, 0.1)",
    BUTTON_COLOR:"rgba(47, 47, 43, 0.2)",
    TEXT_LIGHT:"rgba(47, 47, 43, 0.5)",
    CONTAINER_COLOR: "#2F2F2B",
    BORDER: "#F1F1F1",
    INNER_CONTAINER: "#E5E5E5",
    INPUT_COLOR: "rgba(196, 196, 196, 0.1)",
    BOTTOM_BORDER:"rgba(0, 0, 0, 0.2)",
}


module.exports = COLORS