import { Platform } from "react-native";

const isIOS = Platform.OS === "ios";

const Fonts = {
    small: {
        fontSize: 12,
    },
    smallBold: {
        fontSize: 12,
        fontWeight: '400'
    },
    smallExtraBold: {
        fontSize: 12,
        fontWeight: '700'
    },
    medium: {
        fontSize: 14,
    },
    mediumBold: {
        fontSize: 14,
        fontWeight: '700'
    },
    mediumLightBold: {
        fontSize: 14,
        fontWeight: '400'
    },
    PayableBold: {
        fontSize: 14,
        fontWeight: '600'
    },
    regular: {
        fontSize: 15,
    },
    regularLight: {
        fontSize: 15,
        fontWeight: '400'
    },
    regularBold: {
        fontSize: 15,
        fontWeight: '700'
    },
    mediumLight: {
        fontSize: 16,
        fontWeight: '400'
    },
    mediumWeight: {
        fontSize: 16,
        fontWeight: '600'
    },

    regularLarge: {
        fontSize: 16,
        fontWeight: '700'
    },
    regularFont: {
        fontSize: 16,
    },
    largeLight: {
        fontSize: 18,
        fontWeight: '400'
    },
    large: {
        fontSize: 18,
        fontWeight: '700'
    },
    largeBold:{
        fontSize: 20,
        fontWeight: '600'
    },
    mediumLarge: {
        fontSize: 20,
        fontWeight: '700'
    },
    mediumLargeBold: {
        fontSize: 25,
        fontWeight: '700'
    },
    extraLarge: {
        fontSize: 30,
        fontWeight: '700'
    },
    veryLarge:{
        fontSize: 40,
        fontWeight: '400'
    },
}

module.exports = Fonts