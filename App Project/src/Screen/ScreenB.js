import React, { Component } from 'react'
import { View, Text, Image, FlatList,TextInput, TouchableOpacity, Keyboard ,StyleSheet, ImageBackground} from 'react-native'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux';
import CardView from 'react-native-cardview';
import constants from './constants';
import { getTitle } from '../Redux/Action/titleAction';
import { getDescription } from '../Redux/Action/descriptionAction';
import getAllTitle from '../Redux/middleware/titlemiddleware'; 
import getAllDescription from '../Redux/middleware/descriptionmiddleware'

class ScreenB extends Component {


    constructor() {
        super();
        this.state = {
            names: [
                {
                    name: 'Arithmetic',
                    percentage: '0%'
                }, 
            ]
        };
    }
    async componentDidMount() {
        const { getTitle, title, getDescription,reason} = this.props;
        await getTitle().then(
            this.setState({
                names: title.title,
    
              })
        )
     }
    
    render() {
        const { isChecked, title} = this.state
        const { navigation } = this.props

        return (
            <View style={{flex:1,backgroundColor:constants.Colors. DARK_GRAY}}>
                 
                 <View style={styles.headerBlock} >
                    <TouchableOpacity onPress={()=> navigation.navigate('MainScreen')}>
                    <Image 
                        style={{height:25,width:25}}
                        source={constants.Images.back} />
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Back</Text>
                </View>
              
                <View style={{flex:1}}>
                    <Image source={constants.Images.profile} />
                </View>

                <View style={{marginBottom:70}}>
             <FlatList
                                data={this.state.names}
                                renderItem={({item}) => (
                                    <View style={styles.cardlist}>
                                <CardView
                                    cardElevation={8}
                                    cardMaxElevation={8}
                                    cornerRadius={10}
                                    style={styles.container_reportCard}>
                                   
                                            <Text style={styles.text_display}>{item.title}</Text>
                                      
                                            <Text style={styles.title_style}>{item.description}</Text>
                                       
                                </CardView>
                                </View>
                            )}/>
                            </View>
                           
          </View>
        )
    }
}
const mapStateToProps = (state) => ({
    title:state.title,
    description:state.description
 })
 const mapDispatchToProps = (disaptch) => {
     return {
         getTitle: () => disaptch(getAllTitle()),
         getDescription: () => disaptch(getAllDescription())
     }
 }
const styles= StyleSheet.create({
    cardlist: {
        padding:3,
        marginTop: 20,
    },
    container_reportCard: {
        backgroundColor: 'white',
        marginTop: 3,
        marginLeft:15,
        marginRight:15,
        marginBottom:3,
        padding:5,
    },
    title_style: {
        color: '#515ea1',
        textAlignVertical: 'center',
        fontSize: 14
    },
    text_display: {
        color: '#515ea1',
        textAlignVertical: 'center',
        fontSize: 18,
        fontWeight: 'bold'
    },
    headerBlock: {
        height:60,
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: 10,
    },
    headerText: {
        ...constants.Fonts.large,
        color: constants.Colors.WHITE,
        marginLeft: (constants.BaseStyle.DEVICE_WIDTH / 100) * 3,

    },
})
export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(ScreenB))
