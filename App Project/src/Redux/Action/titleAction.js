export const GET_TITLE = "GET_TITLE"
export const GET_TITLE_ERROR = "GET_TITLE_ERROR"

export function getTitle(data) {
    return {
        type: GET_TITLE,
        payload: data
    }
}

export function getTitleError() {
    return {
        type: GET_TITLE_ERROR,
    }
}