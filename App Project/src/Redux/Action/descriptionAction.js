export const GET_DESCRIPTION = "GET_DESCRIPTION"
export const GET_DESCRIPTION_ERROR = "GET_DESCRIPTION_ERROR"

export function getDescription(data) {
    return {
        type: GET_DESCRIPTION,
        payload: data
    }
}

export function getDescriptionError() {
    return {
        type: GET_DESCRIPTION_ERROR,
    }
}