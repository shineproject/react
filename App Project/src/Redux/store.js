import { createStore, compose, applyMiddleware } from 'redux'
import rootReducer from './Reducer/rootReducer'
import thunk from 'redux-thunk'

function configureStore() {
    let store = createStore(rootReducer, compose(applyMiddleware(thunk)))
    return { store }
}
export default configureStore