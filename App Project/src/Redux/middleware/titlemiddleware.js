import axios from 'axios'
import { getTitle, getTitleError} from '../Action/titleAction'

const getAllTitle = () => {
    return async function (dispatch) {
     return await axios.get("http://15.206.122.102:8000/api/data")
            .then((response) => {
                console.log("Response==========",response.data.data.title)
                // alert(JSON.stringify(response.data.data))
                return (dispatch(getTitle(response.data.data)))
                
            })
            .catch((error) => {
                console.log(error, "middleware error")
                return (dispatch(getTitleError()))
            })
    }
    
}
export default getAllTitle
