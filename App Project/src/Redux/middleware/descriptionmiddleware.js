import axios from 'axios'
import { getDescription, getDescriptionError } from '../Action/descriptionAction'

const getAllDescription = () => {
    return async function (dispatch) {
        return await axios.get("http://15.206.122.102:8000/api/data")
            .then((response) => {
                //console.log(response.data.data.description, "data for get description  iofhsiofkfnknfkdgnlkjfnejhr nserhioe  eiodcnkldeklf")
                return (dispatch(getDescription(response.data.data[0].title)))
               
            })
            .catch((error) => {
                console.log(error, "middleware error")
                return (dispatch(getDescriptionError()))
            })
    }
}


export default getAllDescription
