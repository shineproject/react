import { GET_DESCRIPTION, GET_DESCRIPTION_ERROR } from "../Action/descriptionAction"

const initialState = {
    description: [],
    descriptionError: false
}

export const descriptionReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DESCRIPTION:
            return {
                ...state,
                description: action.payload,
               descriptionError: false
            }
        case GET_DESCRIPTION_ERROR:
            return {
                ...state,
                descriptionError: true
            }

        default:
            return state
    }
}
