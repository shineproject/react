import { GET_TITLE, GET_TITLE_ERROR } from "../Action/titleAction"

const initialState = {
   title: [],
    titleError: false
}

export const titleReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_TITLE:
            return {
                ...state,
                title: action.payload,
                titleError: false
            }
        case GET_TITLE_ERROR:
            return {
                ...state,
                titleError: true
            }

        default:
            return state
    }
}
