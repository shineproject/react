const express = require("express");
const path = require("path");
const app = express();
const port = process.env.PORT || 8000;

require("./db/conn");
const Emp = require("./models/emp");

const hbspath = path.join(__dirname, "../templates/views");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.set("view engine", "ejs");
app.set("views", hbspath);

app.get("/", (req, res) => {
  Emp.find((err, docs) => {
    res.render("index", { clubs: docs });
  }).catch((err) => {
    console.log("mongo not con");
  });
});

app.get("/api/data", (req, res) => {
  Emp.find((err, docs) => {
    // res.render("index", { clubs: docs });
    res.send({success:true, data: docs})
  }).catch((err) => {
    console.log("mongo not con");
  });
});

app.get("/api/edit/:id", (req, res, next) => {
  Emp.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, docs) => {
      if (err) {
        // console.log("db problem");
        // next(err);
        res.json({success:false, message: 'Could not get record, please try later!'})
      } else {
        // res.render("edit", { clubs: docs });
        res.json({success:true, message: 'Data fetched successfully.', data: docs})
      }
    }
  );
});

app.get("/edit/:id", (req, res, next) => {
  Emp.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, docs) => {
      if (err) {
        console.log("db problem");
        next(err);
      } else {
        res.render("edit", { clubs: docs });
      }
    }
  );
});

app.post("/api/edit/:id", (req, res, next) => {
  Emp.findByIdAndUpdate({ _id: req.params.id }, req.body, (err, docs) => {
    if (err) {
      // console.log("not up");
      // next(err);
      res.json({success:false, message: 'Could not update record, please try later!'})
    } else {
      // res.redirect("/");
      Emp.find((err, docss) => {
        // res.render("index", { clubs: docs });
        // res.send({success:true, data: docs})
        res.json({success:true, message: 'Record updated successfully.', data: docss})
      }).catch((err) => {
        console.log("mongo not con");
      });
    }
  });
});

app.post("/edit/:id", (req, res, next) => {
  Emp.findByIdAndUpdate({ _id: req.params.id }, req.body, (err, docs) => {
    if (err) {
      console.log("not up");
      next(err);
    } else {
      res.redirect("/");
    }
  });
});

app.get("/api/del/:id", (req, res, next) => {
  Emp.findByIdAndDelete({ _id: req.params.id }, (err, docs) => {
    if (err) {
      // console.log("not del");
      // next(err);
      res.json({success:false, message: 'Could not delete record, please try later!'})
    } else {
      // console.log("del");
      // res.redirect("/");
      res.json({success:true, message: 'Record deleted successfully.', data: docs})
    }
  });
});

app.get("/del/:id", (req, res, next) => {
  Emp.findByIdAndDelete({ _id: req.params.id }, (err, docs) => {
    if (err) {
      console.log("not del");
      next(err);
    } else {
      console.log("del");
      res.redirect("/");
    }
  });
});

app.post("/", async (req, res) => {
  try {
    const empreg = new Emp({
      title: req.body.title,
      description: req.body.description,
    });
    const registered = await empreg.save();
    res.redirect("/");
  } catch(e) {
    console.log("not save");
    console.log(e);
  }
});

app.post("/api/data", async (req, res) => {
  Emp.create(req.body, (err, docs) => {
    if (err) {
      res.json({success:false, message: 'Could not add record, please try later!'})
    } else {
      // res.redirect("/");
      Emp.find((err, docss) => {
        // res.render("index", { clubs: docs });
        // res.send({success:true, data: docs})
        res.json({success:true, message: 'Record saved successfully.'})
      }).catch((err) => {
        res.json({success:false, message: 'Could not add record, please try later!'})
        console.log("mongo not con");
      });
    }
  });
  // try {
  //   const empreg = new Emp({
  //     title: req.body.title,
  //     description: req.body.description,
  //   });
  //   const registered = await empreg.save();
  //   res.json({success:true, message: 'data saved succesfully'})
  //   //res.redirect("/");
  // } catch(e) {
  //   console.log("not save");
  //   console.log(e);
  //   res.json({success:false, message: e})
  // }
});

app.listen(port, () => {
  console.log("listen ",port);
});