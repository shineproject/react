const mongoose = require("mongoose");

const empschema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: false,
  },
  description: {
    type: String,
    required: true,
  },
  created_on: {
    type: Date,
    default: Date.now,
  },
});

const Emp = new mongoose.model("note", empschema);

module.exports = Emp;
